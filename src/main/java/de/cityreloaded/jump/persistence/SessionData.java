package de.cityreloaded.jump.persistence;

import java.util.Date;

import lombok.Data;

/**
 * Created by Lars on 25.12.2016.
 */
@Data
public class SessionData {

  private String uuid;
  private String trailName;
  private Date lastTry;

  public SessionData(String uuid, String trailName) {
    this(uuid, trailName, new Date());
  }

  public SessionData(String uuid, String trailName, Date lastTry) {
    this.uuid = uuid;
    this.trailName = trailName;
    this.lastTry = lastTry;
  }

}

package de.cityreloaded.jump.persistence;

import de.cityreloaded.jump.Jump;
import de.cityreloaded.jump.trail.Difficulty;
import de.cityreloaded.jump.trail.Trail;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.configuration.ConfigurationSection;

/**
 * Created by Lars on 15.12.2016.
 */
public class TrailSaver {

  public void load() {
    ConfigurationSection trailsSection = Jump.getInstance().getConfig().getConfigurationSection("trails");
    if (trailsSection == null) {
      return;
    }
    trailsSection.getKeys(false).forEach(trailName -> {
      try {
        ConfigurationSection trailSection = trailsSection.getConfigurationSection(trailName);
        int payment = trailSection.getInt("payment");
        Difficulty difficulty = Difficulty.valueOf(trailSection.getString("difficulty"));
        Location start = this.getLocation(trailSection.getConfigurationSection("start"));
        Location end = this.getLocation(trailSection.getConfigurationSection("end"));
        Trail trail = new Trail(trailName, payment, difficulty);
        trail.setStart(start);
        trail.setEnd(end);
        trail.setConfigured(true);

        Jump.getInstance().getTrailManager().addTrail(trail);
      } catch (Exception e) {
        System.out.println(e.getMessage());
      }
    });
  }

  public void save() {
    ConfigurationSection trailsSection = Jump.getInstance().getConfig().createSection("trails");
    Jump.getInstance().getTrailManager().getTrailList().stream().filter(Trail::isConfigured).forEach(trail -> {
      ConfigurationSection trailSection = trailsSection.createSection(trail.getName());
      // OMGEH. This might break. But it might also work
      trailSection.set("payment", trail.getPayment());
      trailSection.set("difficulty", trail.getDifficulty().toString());
      this.setLocation(trailSection.createSection("start"), trail.getStart());
      this.setLocation(trailSection.createSection("end"), trail.getEnd());
    });
  }

  private Location getLocation(ConfigurationSection configSection) throws Exception {
    String worldName = configSection.getString("world");
    double posX = configSection.getDouble("posX");
    double posY = configSection.getDouble("posY");
    double posZ = configSection.getDouble("posZ");
    float yaw = (float) configSection.getDouble("yaw");
    float pitch = (float) configSection.getDouble("pitch");

    World world = Bukkit.getWorld(worldName);

    if (world == null) {
      throw new Exception(String.format("World %s was not found!", worldName));
    }

    return new Location(world, posX, posY, posZ, yaw, pitch);
  }

  private void setLocation(ConfigurationSection configSection, Location location) {
    configSection.set("world", location.getWorld().getName());
    configSection.set("posX", location.getX());
    configSection.set("posY", location.getY());
    configSection.set("posZ", location.getZ());
    configSection.set("yaw", location.getYaw());
    configSection.set("pitch", location.getPitch());
  }

}

package de.cityreloaded.jump.persistence;

import de.cityreloaded.jump.Jump;
import de.cityreloaded.jump.attempt.Attempt;
import de.paxii.mysql.bridge.MySQLBridge;
import de.paxii.mysql.bridge.config.MySQLQueryType;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;

/**
 * Created by Lars on 21.12.2016.
 */
public class TrySaver {

  public SessionData getSessionData(UUID uuid, String trailName) throws SQLException {
    MySQLBridge mySQLBridge = Jump.getInstance().getMySQLBridge();
    String query = mySQLBridge.getQueries().getSelect("SESSION_DATA")[0];
    ResultSet resultSet = mySQLBridge.query(String.format(query, uuid.toString(), trailName)).getResultSet();

    if (!resultSet.isAfterLast() && resultSet.first()) {
      return new SessionData(
              uuid.toString(),
              trailName,
              resultSet.getDate("lasttry")
      );
    }

    return null;
  }

  public void setSessionData(Attempt playerAttempt) throws SQLException {
    MySQLBridge mySQLBridge = Jump.getInstance().getMySQLBridge();
    boolean exists = false;
    try {
      exists = this.getSessionData(playerAttempt.getPlayer().getUniqueId(), playerAttempt.getTrail().getName()) != null;
    } catch (SQLException e) {
      e.printStackTrace();
    }

    if (exists) {
      mySQLBridge.runQuery(
              "SESSION_DATA",
              MySQLQueryType.UPDATE,
              playerAttempt.getPlayer().getUniqueId().toString(),
              playerAttempt.getTrail().getName()
      );
    } else {
      mySQLBridge.runQuery(
              "SESSION_DATA",
              MySQLQueryType.INSERT,
              playerAttempt.getPlayer().getUniqueId().toString(),
              playerAttempt.getTrail().getName()
      );
    }
  }

  public boolean hasTimePassed(UUID uuid, String trailName) {
    try {
      if (this.getSessionData(uuid, trailName) == null) {
        return true;
      }

      MySQLBridge mySQLBridge = Jump.getInstance().getMySQLBridge();
      String query = mySQLBridge.getQueries().getSelect("SESSION_DATA")[1];
      ResultSet resultSet = mySQLBridge.query(String.format(query, uuid.toString(), trailName)).getResultSet();

      if (!resultSet.isAfterLast() && resultSet.first()) {
        return true;
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }

    return false;
  }

}

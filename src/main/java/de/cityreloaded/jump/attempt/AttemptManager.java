package de.cityreloaded.jump.attempt;

import de.cityreloaded.jump.trail.Trail;

import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.UUID;

/**
 * Created by Lars on 11.12.2016.
 */
public class AttemptManager {

  private HashMap<String, Attempt> tryList;

  public AttemptManager() {
    this.tryList = new HashMap<>();
  }

  public boolean isInTrail(String uuid) {
    return this.tryList.containsKey(uuid);
  }

  public boolean isInTrail(UUID uuid) {
    return this.isInTrail(uuid.toString());
  }

  public boolean isInTrail(Player player) {
    return this.isInTrail(player.getUniqueId());
  }

  public boolean addAttempt(Player player, Trail trail) {
    if (!this.isInTrail(player)) {
      Attempt newAttempt = new Attempt(player, trail);
      this.tryList.put(player.getUniqueId().toString(), newAttempt);

      return true;
    }

    return false;
  }

  public Attempt getAttempt(String uuid) {
    return this.tryList.get(uuid);
  }

  public Attempt getAttempt(UUID uuid) {
    return this.getAttempt(uuid.toString());
  }

  public Attempt getAttempt(Player player) {
    return this.getAttempt(player.getUniqueId());
  }

  public boolean removeAttempt(Player player) {
    if (this.isInTrail(player)) {
      this.tryList.remove(player.getUniqueId().toString());
      return true;
    }

    return false;
  }

}

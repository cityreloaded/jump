package de.cityreloaded.jump.attempt;

import de.cityreloaded.jump.Jump;
import de.cityreloaded.jump.persistence.SessionData;
import de.cityreloaded.jump.trail.Trail;

import org.bukkit.Location;
import org.bukkit.entity.Player;

import java.sql.SQLException;

import lombok.Data;

/**
 * Created by Lars on 11.12.2016.
 */
@Data
public class Attempt {
  private Player player;
  private Trail trail;
  private boolean done;
  private Location waypoint;
  private int usedWaypoints;

  public Attempt(Player player, Trail trail) {
    this.player = player;
    this.trail = trail;
  }

  public void setDone(boolean done) {
    this.done = done;

    if (this.done) {
      String prefix = Jump.getInstance().getCommandApi().getChatPrefix();
      SessionData lastSessionData = null;
      try {
        lastSessionData = Jump.getInstance().getTrySaver().getSessionData(
                this.player.getUniqueId(),
                this.trail.getName()
        );
      } catch (SQLException e) {
        e.printStackTrace();
      }

      if (lastSessionData != null) {
        if (Jump.getInstance().getTrySaver().hasTimePassed(this.player.getUniqueId(), this.trail.getName())) {
          try {
            Jump.getInstance().getTrySaver().setSessionData(this);
          } catch (SQLException e) {
            e.printStackTrace();
          }
        } else {
          this.player.sendMessage(String.format(
                  prefix + "Du hast den Kurs %s erfolgreich abgeschlossen!", this.trail.getName())
          );
          this.player.sendMessage(prefix + "Es sind noch keine 2 Wochen vergangen. Du erhälst noch keine Vergütung.");
          return;
        }
      } else {
        try {
          Jump.getInstance().getTrySaver().setSessionData(this);
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
      this.player.sendMessage(String.format(
              prefix + "Du hast den Kurs %s erfolgreich abgeschlossen!", this.trail.getName())
      );
      Jump.getInstance().getVaultApi().addMoney(this.player, this.trail.getPayment());
      this.player.sendMessage(String.format(prefix + "Dir wurden $%d gutgeschrieben.", this.trail.getPayment()));
    }
  }

}

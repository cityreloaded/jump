package de.cityreloaded.jump.command;

import de.cityreloaded.jump.Jump;
import de.cityreloaded.jump.attempt.AttemptManager;
import de.cityreloaded.jump.trail.Trail;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Created by Lars on 13.12.2016.
 */
public class StopCommand extends JumpCommand {

  @Override
  public String getCommand() {
    return "stop";
  }

  @Override
  public String getHelp() {
    return "Beendet einen Jump'n'Run Kurs";
  }

  @Override
  public String getPermission() {
    return "Jump.Stop";
  }

  @Override
  public boolean executeCommand(CommandSender commandSender, String[] args) {
    Player player = Bukkit.getPlayer(commandSender.getName());
    AttemptManager attemptManager = Jump.getInstance().getAttemptManager();

    if (attemptManager.isInTrail(player)) {
      Trail trail = attemptManager.getAttempt(player).getTrail();
      attemptManager.removeAttempt(player);
      player.teleport(trail.getStart());
      this.sendMessage(commandSender, "Der Kurs wurde abgebrochen!");
    } else {
      this.sendMessage(commandSender, "Du bist nicht in einem Jump'n'Run Kurs.");
    }

    return true;
  }

}

package de.cityreloaded.jump.command;

import de.cityreloaded.jump.Jump;
import de.cityreloaded.jump.attempt.Attempt;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Created by Lars on 14.12.2016.
 */
public class BackCommand extends JumpCommand {

  @Override
  public String getCommand() {
    return "back";
  }

  @Override
  public String getHelp() {
    return "Teleportiere dich zu deinem letzten Wegpunkt.";
  }

  @Override
  public String getPermission() {
    return "Jump.Back";
  }

  @Override
  public boolean executeCommand(CommandSender commandSender, String[] args) {
    Player player = Bukkit.getPlayer(commandSender.getName());

    if (Jump.getInstance().getAttemptManager().isInTrail(player)) {
      Attempt playerAttempt = Jump.getInstance().getAttemptManager().getAttempt(player);
      if (playerAttempt.getWaypoint() != null) {
        player.teleport(playerAttempt.getWaypoint());
        playerAttempt.setUsedWaypoints(playerAttempt.getUsedWaypoints() + 1);
        this.sendMessage(commandSender, String.format("Du wurdest du deinem Wegpunkt teleportiert. (%d)", playerAttempt.getUsedWaypoints()));
      } else {
        this.sendMessage(commandSender, "Du hast keinen Wegpunkt gespeichert! Nutze \"/jump save\", um einen Wegpunkt zu erstellen.");
      }
    } else {
      this.sendMessage(commandSender, "Du befindest dich nicht in einem Jump'n'Run Kurs.");
    }

    return true;
  }

}

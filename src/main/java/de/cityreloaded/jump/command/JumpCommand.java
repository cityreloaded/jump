package de.cityreloaded.jump.command;

import de.paxii.bukkit.commandapi.command.AbstractCommand;

/**
 * Created by Lars on 13.12.2016.
 */
public abstract class JumpCommand extends AbstractCommand {

  @Override
  public String[] getRootCommands() {
    return new String[] {
            "jump"
    };
  }

  @Override
  public boolean isPlayerOnly() {
    return true;
  }
}

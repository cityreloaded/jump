package de.cityreloaded.jump.command;

import de.cityreloaded.jump.Jump;
import de.cityreloaded.jump.attempt.AttemptManager;
import de.cityreloaded.jump.trail.Trail;
import de.cityreloaded.jump.trail.TrailManager;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Created by Lars on 13.12.2016.
 */
public class StartCommand extends JumpCommand {

  @Override
  public String getCommand() {
    return "start";
  }

  @Override
  public String getHelp() {
    return "Startet einen Jump'n'Run Kurs";
  }

  @Override
  public String getPermission() {
    return "Jump.Start";
  }

  @Override
  public boolean executeCommand(CommandSender commandSender, String[] args) {
    if (args.length > 0) {
      Player player = Bukkit.getPlayer(commandSender.getName());
      AttemptManager attemptManager = Jump.getInstance().getAttemptManager();
      TrailManager trailManager = Jump.getInstance().getTrailManager();

      if (!attemptManager.isInTrail(player)) {
        if (trailManager.doesTrailExist(args[0])) {
          Trail trail = trailManager.getTrail(args[0]);
          attemptManager.addAttempt(player, trail);
          player.teleport(trail.getStart());
          if (!Jump.getInstance().getTrySaver().hasTimePassed(player.getUniqueId(), trail.getName())) {
            this.sendMessage(commandSender, "Du hast diesen Kurs in den letzten 2 Wochen bereits gespielt. Du erhälst keine Vergütung.");
          }
          this.sendMessage(commandSender, "Viel Spaß!");
        } else {
          this.sendMessage(commandSender, String.format("Der Kurs %s existiert nicht.", args[0]));
        }
      } else {
        this.sendMessage(commandSender, "Du bist bereits in einem Kurs. Benutze \"/jump stop\", um den Versuch abzubrechen.");
      }

      return true;
    }

    return false;
  }

}

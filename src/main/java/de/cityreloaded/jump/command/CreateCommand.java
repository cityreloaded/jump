package de.cityreloaded.jump.command;

import de.cityreloaded.jump.Jump;
import de.cityreloaded.jump.trail.Difficulty;
import de.cityreloaded.jump.trail.Trail;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Arrays;

/**
 * Created by Lars on 13.12.2016.
 */
public class CreateCommand extends JumpCommand {

  @Override
  public String getCommand() {
    return "create";
  }

  @Override
  public String getHelp() {
    return "Erstellt einen Jump'n'Run Kurs";
  }

  @Override
  public String getSyntax() {
    return "create <name> <gewinn> <schwierigkeit>";
  }

  @Override
  public String getPermission() {
    return "Jump.Create";
  }

  @Override
  public boolean executeCommand(CommandSender commandSender, String[] args) {
    if (args.length > 2) {
      Player player = Bukkit.getPlayer(commandSender.getName());

      if (!Jump.getInstance().getTrailManager().doesTrailExist(args[0])) {
        int payment;
        try {
          payment = Integer.parseInt(args[1]);
          if (payment < 0) {
            this.sendMessage(commandSender, "Die Bezahlung kann nicht negativ sein.");
          }
        } catch (NumberFormatException e) {
          this.sendMessage(commandSender, String.format("\"%s\" ist keine gültige Zahl!", args[1]));
          return false;
        }
        Difficulty difficulty = Difficulty.getByString(args[2]);

        if (difficulty == null) {
          this.sendMessage(commandSender, String.format(
                  "\"%s\" ist keine gültige Schwierigkeit (%s)",
                  args[2],
                  String.join(", ", Arrays.stream(Difficulty.values()).map(Difficulty::getDisplayName).toArray(String[]::new)))
          );
          return true;
        }

        Trail trail = new Trail(args[0], payment, difficulty);
        trail.setStart(player.getLocation());
        Jump.getInstance().getTrailManager().createTrail(trail, player);

        this.sendMessage(commandSender, String.format("Der Kurs %s wurde angelegt.", args[0]));
        this.sendMessage(commandSender, "Benutze Linksklick, um eine Druckplatte als Ziel zu markieren.");

        return true;
      } else {
        this.sendMessage(commandSender, String.format("Es existiert bereits ein Kurs mit dem Namen %s.", args[0]));
      }
    }

    return false;
  }

}

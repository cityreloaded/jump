package de.cityreloaded.jump.command;

import de.cityreloaded.jump.Jump;
import de.cityreloaded.jump.attempt.Attempt;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

/**
 * Created by Lars on 14.12.2016.
 */
public class SaveCommand extends JumpCommand {

  @Override
  public String getCommand() {
    return "save";
  }

  @Override
  public String getHelp() {
    return "Speichert deine derzeitige Position. Überschreibt den alten Wegpunkt.";
  }

  @Override
  public String getPermission() {
    return "Jump.Save";
  }

  @Override
  public boolean executeCommand(CommandSender commandSender, String[] args) {
    Player player = Bukkit.getPlayer(commandSender.getName());

    // Cast to Entity to explicitly use the onGround state calculated by the server
    if (((Entity) player).isOnGround()) {
      // TODO: Check world?
      if (Jump.getInstance().getAttemptManager().isInTrail(player)) {
        Attempt playerAttempt = Jump.getInstance().getAttemptManager().getAttempt(player);
        playerAttempt.setWaypoint(player.getLocation());
        this.sendMessage(commandSender, "Dein Wegpunkt wurde gespeichert. Nutze \"/jump back\", um dich dorthin zu teleportieren.");
      } else {
        this.sendMessage(commandSender, "Du befindest dich nicht in einem Jump'n'Run Kurs.");
      }
    } else {
      this.sendMessage(commandSender, "Du musst auf dem Boden stehen, um einen Wegpunkt zu setzen.");
    }

    return true;
  }

}

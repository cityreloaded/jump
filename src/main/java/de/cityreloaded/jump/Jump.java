package de.cityreloaded.jump;

import com.google.gson.Gson;

import de.cityreloaded.jump.command.BackCommand;
import de.cityreloaded.jump.command.CreateCommand;
import de.cityreloaded.jump.command.SaveCommand;
import de.cityreloaded.jump.command.StartCommand;
import de.cityreloaded.jump.command.StopCommand;
import de.cityreloaded.jump.listener.PressurePlateListener;
import de.cityreloaded.jump.persistence.TrailSaver;
import de.cityreloaded.jump.persistence.TrySaver;
import de.cityreloaded.jump.attempt.AttemptManager;
import de.cityreloaded.jump.trail.TrailManager;
import de.paxii.bukkit.commandapi.CommandApi;
import de.paxii.bukkit.vaultapi.VaultApi;
import de.paxii.mysql.bridge.MySQLBridge;
import de.paxii.mysql.bridge.config.MySQLDetails;
import de.paxii.mysql.bridge.config.MySQLQueryType;

import org.bukkit.ChatColor;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;

import lombok.Getter;

/**
 * Created by Lars on 11.12.2016.
 */
public class Jump extends JavaPlugin {

  @Getter
  private static Jump instance;

  @Getter
  private TrailManager trailManager;

  @Getter
  private AttemptManager attemptManager;

  private TrailSaver trailSaver;

  @Getter
  private TrySaver trySaver;

  @Getter
  private CommandApi commandApi;

  @Getter
  private VaultApi vaultApi;

  @Getter
  private MySQLBridge mySQLBridge;

  // TODO: Add Commands to manage Trails (remove)

  public Jump() {
    Jump.instance = this;
    this.trailManager = new TrailManager();
    this.attemptManager = new AttemptManager();
    this.trailSaver = new TrailSaver();
    this.mySQLBridge = new MySQLBridge(this.getClass());
    this.trySaver = new TrySaver();
    this.vaultApi = new VaultApi();
    this.commandApi = new CommandApi();
  }

  @Override
  public void onEnable() {
    Gson gson = new Gson();
    this.commandApi.setChatPrefix(ChatColor.GOLD + "[Jump] " + ChatColor.RESET);

    this.getServer().getPluginManager().registerEvents(new PressurePlateListener(), this);

    this.commandApi.addCommand(new SaveCommand());
    this.commandApi.addCommand(new StopCommand());
    this.commandApi.addCommand(new BackCommand());
    this.commandApi.addCommand(new StartCommand());
    this.commandApi.addCommand(new CreateCommand());

    this.getCommand("jump").setExecutor(this.commandApi.getExecutor());

    if (!this.vaultApi.setupEconomy()) {
      System.out.println(String.format("[%s] Could not find Vault, disabling plugin.", this.getName()));
      this.setEnabled(false);
    }

    try {
      File mySQLData = new File(this.getDataFolder(), "MySQL.json");
      if (!mySQLData.exists()) {
        if (mySQLData.createNewFile()) {
          System.out.println("MySQL.json not found, creating new one...");
        } else {
          System.out.println(mySQLData.getAbsolutePath() + " could not be created.");
        }
        this.setEnabled(false);
        return;
      }

      this.mySQLBridge.setConnectionData(
              gson.fromJson(this.mySQLBridge.getFileBridge().getFileContents(mySQLData), MySQLDetails.class)
      );
      this.mySQLBridge.initConnection();


      if (!Jump.getInstance().getMySQLBridge().doesTableExist("sessiondata")) {
        Jump.getInstance().getMySQLBridge().runAllQueries("SESSION_DATA", MySQLQueryType.CREATE);
      }
    } catch (Exception exception) {
      exception.printStackTrace();
    }

    this.getServer().getScheduler().runTaskLater(this, () -> {
      System.out.println("Loading Jump Trails");
      this.trailSaver.load();
    }, 40L);
  }

  @Override
  public void onDisable() {
    this.mySQLBridge.closeConnection();
    this.trailSaver.save();
    this.saveConfig();
    PlayerInteractEvent.getHandlerList().unregister(this);
  }

}

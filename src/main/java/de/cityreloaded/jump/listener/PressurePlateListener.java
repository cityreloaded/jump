package de.cityreloaded.jump.listener;

import de.cityreloaded.jump.Jump;
import de.cityreloaded.jump.attempt.Attempt;
import de.cityreloaded.jump.trail.Trail;
import de.cityreloaded.jump.trail.TrailManager;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

/**
 * Created by Lars on 13.12.2016.
 */
public class PressurePlateListener implements Listener {

  @EventHandler
  public void onPressurePlateClicked(PlayerInteractEvent playerInteractEvent) {
    if (playerInteractEvent.getAction() == Action.LEFT_CLICK_BLOCK) {
      if (playerInteractEvent.getClickedBlock().getType().toString().endsWith("_PLATE")) {
        TrailManager trailManager = Jump.getInstance().getTrailManager();

        if (trailManager.isCreatingTrail(playerInteractEvent.getPlayer())) {
          Trail trail = trailManager.getCreatingTrail(playerInteractEvent.getPlayer());
          trail.setEnd(playerInteractEvent.getClickedBlock().getLocation());
          trailManager.setTrailCreated(playerInteractEvent.getPlayer());
          playerInteractEvent.getPlayer().sendMessage(
                  Jump.getInstance().getCommandApi().getChatPrefix() +
                          "Der Kurs wurde erfolgreich eingerichtet!"
          );
          playerInteractEvent.setCancelled(true);
        }
      }
    }
  }

  @EventHandler
  public void onPressurePlateUsed(PlayerInteractEvent playerInteractEvent) {
    if (playerInteractEvent.getAction() == Action.PHYSICAL) {
      if (playerInteractEvent.getClickedBlock().getType().toString().endsWith("_PLATE")) {
        if (Jump.getInstance().getAttemptManager().isInTrail(playerInteractEvent.getPlayer())) {
          Attempt playerAttempt = Jump.getInstance().getAttemptManager().getAttempt(playerInteractEvent.getPlayer());
          Trail trail = playerAttempt.getTrail();

          if (trail.getEnd().equals(playerInteractEvent.getClickedBlock().getLocation())) {
            playerAttempt.setDone(true);
            Jump.getInstance().getAttemptManager().removeAttempt(playerInteractEvent.getPlayer());
          }
        }
      }
    }
  }

}

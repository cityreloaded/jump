package de.cityreloaded.jump.trail;

import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.HashMap;

import lombok.Getter;

/**
 * Created by Lars on 11.12.2016.
 */
public class TrailManager {

  @Getter
  private ArrayList<Trail> trailList;
  private HashMap<String, String> creatingTrail;

  public TrailManager() {
    this.trailList = new ArrayList<>();
    this.creatingTrail = new HashMap<>();
  }

  public boolean addTrail(Trail trail) {
    if (!this.doesTrailExist(trail.getName())) {
      this.trailList.add(trail);
      return true;
    }

    return false;
  }

  public boolean createTrail(Trail trail, Player player) {
    if (this.addTrail(trail)) {
      this.creatingTrail.put(player.getUniqueId().toString(), trail.getName());
      return true;
    }

    return false;
  }

  public boolean removeTrail(String name) {
    if (this.doesTrailExist(name)) {
      Trail trail = this.getTrail(name);
      this.trailList.remove(trail);
      return true;
    }

    return false;
  }

  public boolean doesTrailExist(String name) {
    return this.getTrail(name) != null;
  }

  public Trail getTrail(String name) {
    for (Trail trail : this.trailList) {
      if (trail.getName().equalsIgnoreCase(name)) {
        return trail;
      }
    }

    return null;
  }

  public boolean isCreatingTrail(Player player) {
    return this.creatingTrail.containsKey(player.getUniqueId().toString());
  }

  public Trail getCreatingTrail(Player player) {
    return this.getTrail(this.creatingTrail.get(player.getUniqueId().toString()));
  }

  public void setTrailCreated(Player player) {
    Trail trail = this.getCreatingTrail(player);
    trail.setConfigured(true);
    this.creatingTrail.remove(player.getUniqueId().toString());
  }

}

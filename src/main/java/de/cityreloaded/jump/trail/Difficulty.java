package de.cityreloaded.jump.trail;

import lombok.Getter;

/**
 * Created by Lars on 11.12.2016.
 */
public enum Difficulty {
  EASY("Einfach"),
  MEDIUM("Mittel"),
  HARD("Schwer");

  @Getter
  private String displayName;

  Difficulty(String displayName) {
    this.displayName = displayName;
  }

  public static Difficulty getByString(String input) {
    for (Difficulty difficulty : values()) {
      // FIXME Remove substring check as soon as we have multiple Difficulties with same first char
      if (difficulty.displayName.equalsIgnoreCase(input)
              || difficulty.displayName.substring(0, 1).equalsIgnoreCase(input)) {
        return difficulty;
      }
    }

    return null;
  }

}

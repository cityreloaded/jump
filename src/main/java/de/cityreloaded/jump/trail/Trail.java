package de.cityreloaded.jump.trail;

import org.bukkit.Location;

import lombok.Data;

/**
 * Created by Lars on 11.12.2016.
 */
@Data
public class Trail {

  private String name;
  private int payment;
  private boolean configured = false;
  private Difficulty difficulty;
  private Location start;
  // Should this be an area?
  // Maybe we should just remove this and use a pressure plate / button to mark the trail as done?
  private Location end;

  public Trail(String name, int payment, Difficulty difficulty) {
    this.name = name;
    this.payment = payment;
    this.difficulty = difficulty;
  }

}
